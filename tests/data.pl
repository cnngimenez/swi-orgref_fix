/*   data
     Author: poo.

     Copyright (C) 2020 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     11 Jul 2020
*/


:- module(data, [
              chang_html/1,
              lst_citations/1,
              bibliography_html/1,
              cites_expected/1,
              example_bibtex/1
	  ]).
/** <module> data: Common test data


@license GPLv3
@author Christian Gimenez
*/

:- license(gplv3).

:- use_module(library(bibtex)).

bibliography_html(Html) :-
    read_file_to_string('data/bibliography.html', Html, []).

lst_citations([
                     "schneider73:_cours_modul_applied",
                     "chang18:_scaling_knowled_access",
                     "arun18:_making_search_easier",
                     "pittman17:_cracking_code_conver",
                     "noy19:_industr_scale_knowled",
                     "deepika17:_happy_birthd_watson",
                     "qi16:_buildin_linked_knowled",
                     "saurabh17:_bring_rich_knowled",
                     "ferras18:_food_discov_uber",
                     "chang18:_scaling_knowled_access",
                     "giese15:optique",
                     "tudorache13:webprotege",
                     "ozgur:_ref_ex"
                 ]).



chang_html("  <div>
    <a name=\"chang18:_scaling_knowled_access\" class=\"org-bibitem\" ></a>
    <p class=\"org-bibitem\">
      Spencer Chang. Scaling Knowledge Access and Retrieval at Airbnb (2018).
    </p>
  </div>
").

%% bibtex_get_entries/3 returns the entries in inverted according
%% to the bibtex order. Thus, this must be specified in that manner.
cites_expected(
    [
        cite("ozgur:_ref_ex",
             "(Özçep, 2020)"),
        cite("tudorache13:webprotege",
             "(Tudorache <i>et al.</i>, 2013)"),
        cite("giese15:optique",
            "(Giese <i>et al.</i>, 2015)"),
        cite("schneider73:_cours_modul_applied",
             "(Schneider, 1973)"),
        cite("saurabh17:_bring_rich_knowled",
             "(Shrivastava, 2017)"),
        cite("qi16:_buildin_linked_knowled",
             "(He <i>et al.</i>, 2016)"),
        cite("pittman17:_cracking_code_conver",
             "(Pittman <i>et al.</i>, 2017)"),
        cite("noy19:_industr_scale_knowled",
             "(Noy <i>et al.</i>, 2019)"),
        cite("ferras18:_food_discov_uber",
             "(Ferras Hamad, 2018)"),
        cite("deepika17:_happy_birthd_watson",
             "(Devarajan, 2017)"),
        cite("chang18:_scaling_knowled_access",
             "(Chang, 2018)"),
        cite("arun18:_making_search_easier",
             "(Krishnan, 2018)")
    ]).

example_bibtex(Lst_entries) :-
    lst_citations(Lst_cites), 
    bibtex_get_entries('data/biblio.bib', Lst_cites, Lst_entries).

