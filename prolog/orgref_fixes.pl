/*   orgref_fixes
     Author: Christian Gimenez.

     Copyright (C) 2020 Christian Gimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     11 Jul 2020
*/


:- module(orgref_fixes, [
              fix_citations/4,
              fix_references/3,
              fix_all/4,

              generate_bibliography/3,
              generate_cites/4
	  ]).
/** <module> orgref_fixes: Fix an org file with org-ref citations and references.


@license GPLv3
@author Christian Gimenez
*/

:- license(gplv3).

:- use_module(library(dcg/basics)).
:- use_module(orgref_search).
:- use_module(library(bibtex)).
:- use_module(library(bibtex_fields)).

%% --------------------
%% Main predicates
%% --------------------

/**
 fix_citations(+Org: term, +Bibtex: term, +Html: string, -Html_output: string)

Fix citation links and insert the bibliography at the end.

@param Org The org file path.
@param Bibtex The bibtex file path.
@param Html the Html export string. Use read_file_to_string/3 to read a 
  whole file.
*/
fix_citations(Org, Bibtex, Html, Html_output) :-
    read_file_to_string(Org, OrgText, []),
    
    search_citations(OrgText, Lst_cites),
    generate_cites(Bibtex, Lst_cites, Lst_citemaps, Lst_entries),
    
    html_fix_citations(Lst_citemaps, Html, Html1),
    insert_bibliography(Lst_entries, Html1, Html_output).

/**
 fix_references(Org, Html, Html_output).

Fix references to a figure, table and/or section.

@tbd
*/
fix_references(_Org, Html, Html).
% search_refs(Org, Lst_refs),
% replace_refs(Lst_refs, Html).

/**
 fix_all(+Org_file: term, +Bibtex: term, +Html_file: term, +Result_file: term) 

Fix the following items on the exported Html_file:

- fix_references/3
- fix_citations/4

@param Org_file The org file path.
@param Bibtex The bibtex file path.
@param Html_file The HTML exported file.
@param Result_file The HTML output file which will be created.
*/
fix_all(Org_file, Bibtex, Html_file, Result_file) :-
    read_file_to_string(Html_file, Html, []),
    
    fix_references(Org_file, Html, Html1),
    fix_citations(Org_file, Bibtex, Html1, Html2),

    tell(Result_file),
    write(Html2),
    told.

%% --------------------
%% Auxiliary predicates
%% --------------------

/**
 et alii(-Author_value: string, Abbrv: string)

From a BibTeX author value return the abbreviated string, suitable for an
APA-styled reference.

For example: from "Tania Tudorache and Csongor Nyulas and Natalya Fridman Noy
 and Mark A. Musen" return "Tudorache <i>et al.</i>". 

The "et al." is added when more than one author is founded. Else, the author
surname is used.
*/
et_alii(Author_value, Abbrv) :-
    author_field(field(author, Author_value), Authors),
    et_alii_int(Authors, Abbrv), !.

et_alii_int([author(Surname, _Name)], Value) :-
    format(string(Value), "~s", [Surname]), !. % red cut.
et_alii_int([author(Surname, _Name)|_Rest], Value) :-
    format(string(Value), "~s <i>et al.</i>", [Surname]), !. % red cut.    

/**
 accent(-Accent_type: string)//

Parse the next character and return the type of accent founded.

@param Accent_type A string with the accent type.
*/
accent("'") --> [39].
accent("`") --> [96].
accent("\"") --> [34].
accent("~") --> `~`.
accent("c") --> `c`.
accent("s") --> `s`.                            
accent("a") --> `a`.
accent("A") --> `A`.
              
/**
 accented_vowel(+Accent_type: string , -Accented_vowel: codes)//

Parse the next vowel and add the accent in Accent_type. Accent_type can be
obtained from accent//1.

@param Accent_type The LaTeX accent type. For instance, for the LaTeX "\'"
  accent use `Accent_type = "'"`.
@param Accented_vowel The resulting code: the vowel with the accent in one 
  character.

@see accent//1
*/
accented_vowel("'", `á`) --> "a".
accented_vowel("'", `é`) --> "e".
accented_vowel("'", `í`) --> "i".
accented_vowel("'", `ó`) --> "o".
accented_vowel("'", `ú`) --> "u".
accented_vowel("'", `Á`) --> "A".
accented_vowel("'", `É`) --> "E".
accented_vowel("'", `Í`) --> "I".
accented_vowel("'", `Ó`) --> "O".
accented_vowel("'", `Ú`) --> "U".
accented_vowel("`", `à`) --> "a".
accented_vowel("`", `è`) --> "e".
accented_vowel("`", `ì`) --> "i".
accented_vowel("`", `ò`) --> "o".
accented_vowel("`", `ù`) --> "u".
accented_vowel("`", `À`) --> "A".
accented_vowel("`", `È`) --> "E".
accented_vowel("`", `Ì`) --> "I".
accented_vowel("`", `Ò`) --> "O".
accented_vowel("`", `Ù`) --> "U".
accented_vowel("\"", `ä`) --> "a".
accented_vowel("\"", `ë`) --> "e".
accented_vowel("\"", `ï`) --> "i".
accented_vowel("\"", `ö`) --> "o".
accented_vowel("\"", `ü`) --> "u".
accented_vowel("\"", `Ä`) --> "A".
accented_vowel("\"", `Ë`) --> "E".
accented_vowel("\"", `Ï`) --> "I".
accented_vowel("\"", `Ö`) --> "O".
accented_vowel("\"", `Ü`) --> "U".
accented_vowel("~", `ñ`) --> "n".
accented_vowel("~", `Ñ`) --> "N".
accented_vowel("c", `ç`) --> "c".
accented_vowel("s", `ß`) --> "s".
accented_vowel("A", `Å`) --> "A".
accented_vowel("a", `å`) --> "a".
accented_vowel("a", `æ`) --> "e".


/**
 parse_latex(-PlainText: codes)//

DCG rule to parse a LaTeX fragment and generate a plain text without:

- Curly brackets.
- Accents
- Excesive whitespaces.

@param PlainText The results of the DCG parsing and convertion.

@see latex2text/2
*/
parse_latex([B|Rest]) -->
    %% Remove \'{...}, \c{...}, \~{...} and \"{...} accents.
    `\\`, accent(A), `{`, accented_vowel(A, [B]), `}`, !,
    parse_latex(Rest).
parse_latex([B|Rest]) -->
    %% Remove \'..., \c..., \~... and \"... accents.
    `\\`, accent(A), accented_vowel(A, [B]), !,
    parse_latex(Rest).
parse_latex(Text) -->
    %% Remove { and }
    `{`, parse_latex(Nested), `}`, !, parse_latex(Rest),
    {append(Nested, Rest, Text)}.
parse_latex([32|Rest]) -->
    %% Just add one whitespace if tab or blanks are founded.
    blank, blanks, parse_latex(Rest).
parse_latex([C|Rest]) -->
    %% Add the rest of the characters.
    [C], parse_latex(Rest),
    {[C] \= `}`}, !.
parse_latex([]) --> [].

/**
 latex2text(+Latex: string, -Text: string)
 latex2text(+latex: codes, -Text: string)

Convert LaTeX accents, remove curly brackets and excesive spaces into text.
For instance, convert the following LaTeX text

```
La   herramienta {Prot\'eg{\'{é}}} es utilizada para
        la   {W}eb  {Sem\'{a}ntica}.
```

into a plaint text as follows:

```
La herramienta Protégé es utilizada para la Web Semántica.
```

This is useful for generating the bibliography or reference text from the
BibTeX's author names.

@param Latex A string or codes to convert.
@param Text The results of the convertion.
*/
latex2text(Latex, Text) :-
    is_of_type(string, Latex), !,
    
    string_codes(Latex, Latex_codes),
    phrase(parse_latex(Text1), Latex_codes),
    string_codes(Text, Text1).

latex2text(Latex, Text) :-
    is_of_type(codes, Latex),
    phrase(parse_latex(Text1), Latex),
    string_codes(Text, Text1).

/**
 get_field(+FieldName: atom, +Fields: list, -Value: string)

Return the field value from an entry/3's field/2 data.

This predicate always is succeds.

@param FieldName The field name to retrieve. Ex.: `author`.
@param Fields The list of field/2 terms.
@param Value The string value. An empty string if the field does not exists.
*/
get_field(FieldName, Fields, Value) :-
    member(field(FieldName, Value), Fields), !. %% red cut!
get_field(_Fieldname, _Fields, ""). % <- Couldn't find this field

%% --------------------
%% Citations references
%% --------------------

/**
 generate_cite(-Entry: term, -Cite_map: term) is det

Generate the cite/2 term from the given entry. The term consist of:
`cite(Entry_label: string, Reference_string: string)`.

For example, it generate the following "citemap" from an entry:
```
cite("schneider73:_cours_modul_applied", "(Schneider, Edward W, 1973)").
```

@param Entry An entry/3 term.
@param Cite_map A cite/2 term.
*/
generate_cite(entry(_Key, Label, Fields), cite(Label, Str)):-
    get_field(author, Fields, Author_value),
    latex2text(Author_value, Author_value2),
    et_alii(Author_value2, Auth_abbrv),
    get_field(year, Fields, Year),
    format(string(Str), "(~s, ~s)", [Auth_abbrv, Year]).

/**
 fold_mapcites_pred(+Entry: term, +Lst_prev: list, -Lst_next: list)

Predicate used by a foldl/4 call. Generate the "citemap" from the given Entry
and append it to the previous list Lst_prev. 

@param Entry An entry/3 term.
@param Lst_prev A list of cite/2 citemaps.
@param Lst_next A list of resulting cite/2 citemaps.
*/
fold_mapcites_pred(Entry, Lst_prev, [Map|Lst_prev]) :-
    generate_cite(Entry, Map).    

/**
 generate_mapcites(+Lst_entries: list, -Lst_maps: list)

Generate citemaps from the given entry. Each citemap is used to generate the
citation references in the middle of the text.

For instance, if a bibtex key is founded in the middle of the text, it should
be replaced by the correct APA (or other style) citation reference.

@param Lst_entries A list of entry/3 terms given by any bibtex predicate
  library.
@param Lst_maps A list of cite/2 terms.
*/
generate_mapcites(Lst_entries, Lst_maps) :-
    foldl(fold_mapcites_pred, Lst_entries, [], Lst_maps).

/**
 generate_cites(+Bibtex: term, +Lst_cites: list, -Map: list, -Lst_entries: list).

Given a list of bibtex citation keys, search each entry on the Bibtex file
and create the citation maps.

@param Bibtex The bibtex file path.
@param Lst_cites A list of strings with cite keys.
@param Map A cite/2 map between each key and the APA reference string.
@param Lst_entries The list of bibtex entry/3 entries.
*/                      
generate_cites(Bibtex, Lst_cites, Map, Lst_entries) :-
    bibtex_get_entries(Bibtex, Lst_cites, Lst_entries),
    generate_mapcites(Lst_entries, Map).

/**
 replace_cites(+Citemap: term, +Html: string, -Html_output: string)

For the `Citemap = cite(Citekey, Labelstr)`, replace every anchor with href to
with a proper formatted anchor with the Labelstr as text.

@param Citemap A cite/2 mapping `cite(+Citekey:string, +Labelstr:string)`.
@param Html The org export as string.
@param Html_output The replaced org.
*/
replace_cites(cite(Key, Str), Html, Html_output) :-
    format(string(Regexp), '<a href="([^"]+)">~s</a>', [Key]),
    format(string(Result),
           '<a href="#\\1" class="cite-link" data-ref="~s">~s</a>',
           [Key, Str]),
    re_replace(Regexp/g, Result, Html, Html_output).

/**
 html_fix_citations(+Lst_citemaps: list, +Html: string, -Html_output: string)

Fix the citation references.

@param Lst_citemaps A list of cite/2 mappings.
@param Html A string with the exported org file.
@param Html_output A string with the fixed anchors.
*/
html_fix_citations(Lst_citemaps, Html, Html_output) :-
    foldl(replace_cites, Lst_citemaps, Html, Html_output).



%% --------------------
%% Bibliography printing
%% --------------------

/**
 process_field(+Fields: list, -Text: string)

Create the bibliography line by using the BibTeX field list.

For example, from an `entry(Key, Label, Fields)` create the following
APA-styled bibliography line format:

`AuthorSurname, AuthorName. Title (Year).`

The fields taken from the Fields list are the following:

- author
- year
- title

@param Fields The field list from the BibTeX entry/3 term.
@param Text The string produced.
*/
process_field(Fields, Html) :-
    get_field(author, Fields, Author),
    get_field(year, Fields, Year),
    get_field(title, Fields, Title),

    latex2text(Author, AuthorText),
    latex2text(Title, TitleText),

    format(string(Html), '~s. ~s (~s).', [
               AuthorText, TitleText, Year
           ]).

/**
 generate_html(+Entry: term, -Html: string)

Generate an HTML bibliography div from a BibTeX entry. 
The format produced is an HTML as follows:

```
  <div>
    <a name="Entry_label" class="org-bibitem" ></a>
    <p class="org-bibitem">
      -- APA-styled bibliography reference here --
    </p>
  </div>
```

@param Entry A BibTeX entry/3 as the one produced by the bibtex library.
@param Html The HTML filled with the BibTeX entry/3 data as described before.
*/
generate_html(entry(_Key, Label, Fields), Html) :-
    process_field(Fields, Fields_HTML),
    format(string(Html),
           '  <div>
    <a name="~s" class="org-bibitem" ></a>
    <p class="org-bibitem">
      ~s
    </p>
  </div>
',
           [
               Label,
               Fields_HTML
           ]).

/**
 fold_bibs_pred(+Entry: term, +PrevHtml: string, -NextHtml: string)

Internal predicate. It is used by a foldl/4 predicate to generate all the 
bibliography HTML items.

Generate a bibliography APA-styled text from the given Entry. Use a HTML
syntax.

@param Entry A BibTeX entry/3 term where to take the author, title and year for
  the bibliography text.
@param PrevHtml The previous HTML string generated by the previous call.
@param NextHtml The output HTML for the next call.

@see generate_bibs_html/2
*/
fold_bibs_pred(Entry, PrevHtml, NextHtml) :-
    generate_html(Entry, Html),
    string_concat(PrevHtml, Html, NextHtml).

/**
 generate_bibs_html(+Lst_entries: list, -Html: string)

Generate an APA-styled bibliography from the given entry/3 BibTeX list. The
output is formated using HTML syntax.

@param Lst_entries A list of entry/3 BibTeX entries.
@param Html The HTML bibliography text generated in HTML format.
*/
generate_bibs_html(Lst_entries, Html) :-
    foldl(fold_bibs_pred, Lst_entries, "\n<div class=\"org-bib\">\n", Html1),
    string_concat(Html1, "</div><!-- /org-bib -->\n", Html).

/**
 generate_bibliography(+Bibtex: term, +Lst_cites: list, -Html: string)

Generate an APA-styled bibliography in HTML syntax from a list of citation
labels.

@param Bibtex The BibTeX file path.
@param Lst_cites A list of strings. These must be citation labels
  (ex. `["giese15:optique", "chang18:_scaling_knowled_access"]`).
@param Html The HTML bibliography text generated in HTML format.
*/
generate_bibliography(Bibtex, Lst_cites, Html) :-
    bibtex_get_entries(Bibtex, Lst_cites, Lst_entries),
    generate_bibs_html(Lst_entries, Html).

/**
 replace_printbibliography(+Html: string, +Bib_html: string, 
   -Html_output: string)

Find all `\printbibliography` text in Html and replace it with Bib_Html.

@param Html The input HTML text.
@param Bib_html The bibliography in HTML format.
@param Html_output The output HTML text
*/
replace_printbibliography(Html, Bib_html, Html_output) :-
    %% Avoid interpreting \NAME in the next re_replace/4. Bib_html2 is
    %% used as a second pararemeter, and all "\NAME" is enterpreted as
    %% a named group match. Add accent_vowel/2 if any LaTeX code is
    %% not parsed properly.
    re_replace('\\\\'/g, "\\\\\\\\", Bib_html, Bib_html2), 
    re_replace('\n[[:space:]]*\\\\printbibliography[[:space:]]*\n'/g,
               Bib_html2, Html, Html_output).

/**
 insert_bibliography(+Lst_entries: list, +Html: string, -Html_output: string)

Replace the `\printbibliography` text in the Html string with the bibliography 
generated from the BibTeX entries given.

The bibliography text used is an HTML produced by the generate_bibs_html/2
predicate.

@param Lst_entries A list of BibTeX entry/3 terms as returned by 
  bibtex_get_entries/3 from the bibtex library.
@param Html The HTML input.
@param Html_output The HTML output with the bibliography inserted where the
  `\printbibliography` text were. 
*/
insert_bibliography(Lst_entries, Html, Html_output) :-
    generate_bibs_html(Lst_entries, Bib_html),
    replace_printbibliography(Html, Bib_html, Html_output).

