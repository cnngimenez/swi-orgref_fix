/*   orgref_search.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     06 Jun 2020
*/


:- module(orgref_search, [
              search_citations/2,
              search_refs/2
	  ]).
/** <module> orgref_search: Search citations and references on an org file.


@license GPLv3
@author Christian Gimenez
*/

:- license(gplv3).

:- use_module(library(bibtex)).


:- dynamic bibs/1.

load_bibtex(File) :-
    bibtex_file(File, L),
    retractall(bibs/1),
    assertz(bibs(L)).

:- use_module(library(dcg/basics)).

/** 
  citation(-Link: codes)//

How the citation is formatted by ox-twbs or ox-html export.
*/
citation(Link) --> "<a href=\"", string_without("\"", Link), "\" >".

a_tag_regexp(CiteKey, Tag) :-
    string_concat("<a href=\"", CiteKey, S1),
    string_concat(S1, "\">([^<]+)</a>", Tag).    

a_tag_converted(CiteKey, CiteLabel, New_tag) :-
    format(string(New_tag), "<a href=\"~s\">~s</a>", [CiteKey, CiteLabel]).

%% fix_citations(CiteKey, CiteLabel, Html, Html2) :-
%%     a_tag_regexp(CiteKey, A_tag),
%%     a_tag_converted(CiteKey, CiteLabel, New_tag),
%%     re_replace(A_tag, New_tag, Html, Html2).


citation_regexp("(parencite|cite):([^\\s\\.,;]+)").
ref_regexp("(ref):([^\\s\\.,;]+)").

/**
 join(+Match, +V0, -V1) 

Predicate used to join matches used by re_foldl/6.

@param Match A re_match.
@param V0 The previous collected list.
@param V1 The next list with the Match included.
*/
join(Match, V0, [Match|V0]).


/**
 search_regcites(+Org: string, -Lst_matches: list)

@param Org The string where to search for citations.
@param Lst_matches A list of dicts of number to string. Each number is 
  the group matched. The zero number is the global match.
*/
search_regcites(Org, Lst_matches) :-
    citation_regexp(Regexp),
    re_foldl(join, Regexp, Org, [], Lst_matches, []).

search_references(Org, Lst_matches) :-
    ref_regexp(Regexp),
    re_foldl(join, Regexp, Org, [], Lst_matches, []).


/**
 get_keys(+Lst_matches: list, -Lst_keys: list)

Collect the citation or reference keys from the re_match dicts. 

The second group of the dict match must be the key.

@param Lst_matches A re_match dict list.
@param Lst_keys A list of strings with the citation keys.
*/
get_keys([], []) :- !.
get_keys([Match|RM], [Key|RL]) :-
    .(Match, 2, Key),
    get_keys(RM, RL).

/**
 search_citations(+Org: string, -Lst_cites: list)

Search for the citation keys used in the Org text. This citations have got
the format expressed by the regexp citation_regexp/1. 

Look for all of those "cite" and "parencite" and return only the key strings
on Lst_cites.

@param Org The whole Org file in string format.
@param Lst_cites A list of citation key strings.
*/
search_citations(Org, Lst_cites) :-
    search_regcites(Org, Matches),
    get_keys(Matches, Lst_cites1),
    %% Return it in citation order.
    reverse(Lst_cites1, Lst_cites).

/**
 search_refs(+Org: string, -Lst_refs: list)

Search for all the references and return their keys on Lst_refs.

@param Org The whole Org file in string format.
@param Lst_refs A list of reference key strings
*/
search_refs(Org, Lst_refs) :-
    search_references(Org, Matches),
    get_keys(Matches, Lst_cites1),
    reverse(Lst_cites1, Lst_refs).



