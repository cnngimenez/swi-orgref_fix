Referenced packages in this documentation:

org-ref:
[![img](https://melpa.org/packages/org-ref-badge.svg)](https://melpa.org/#/org-ref)
ox-twbs (optional):
[![img](https://melpa.org/packages/ox-twbs-badge.svg)](https://melpa.org/#/ox-twbs) 


# Org-ref Fix

When using [Org-mode](https://orgmode.org) with [org-ref](https://melpa.org/#/org-ref), beautiful references are generated on LaTeX exports. But what about HTML? References and citations are exported as broken links, specially if you use the [ox-twbs](https://melpa.org/#/ox-twbs) package (from Melpa) or ox-html (included in Org-mode and Emacs).

So, this package provides predicates to fix this and more. This is what this package can do for you:

-   Search for references and citations from an Org-file.
-   Take those citations, the BibTeX file and retrieve their entries.
-   Generate the bibliography section in HTML.
-   Fix all citation reference links to point to the bibliography item properly.
-   Fix all citation reference texts to a proper somewhat simmilar APA label.
-   Fix all references to figures, tables and footnotes (:warning: not yet implemented!).


# How to use?


## Installation

First, install the package in SWI-Prolog. Run `swipl` on your terminal and write:

    pack_install(orgref_fix).


## Export your org document into HTML

Use Emacs to export your org document into HTML. Use `C-c C-e h h` for HTML or `C-c C-e w h`  for TWBS export.

Ensure that `\printbibliography` string is where you want the bibliography or references in your HTML. It will be replaced by the HTML code with the author and titles. If it is not exported in your HTML code, add it by hand or try the following in your org file and export to HTML again:

    #+HTML: \printbibliography


## Use this library

Load the `orgref_fixes` library and execute `fix_all/4` predicate. For example, to fix the citations, references and to add the bibliography to the HTML export file from `org_file.org`, write the following:

    use_module(library(orgref_fixes)).
    fix_all('org_file.org', 'bibtex_file.bib', 'exported_html_file.html', 'output_file.html').


# Some problems


## Accents and Non-ASCII letters

Author names and titles may have non-ascii letters. This prolog code tries to recognise some of LaTeX commands that generates these characters, such as: `\'{a}` for á, `\'{e}` for é, and others: íóúñäëïöüçßÅåæ. Any LaTeX commands that were not interpreted by the code, are leaved as it is without curly bracket ("{" and "}") characters.

To add more support, you can download this code and add more accents by modifying the `accent//1` and `accented_vowel//2` DCG rules. The first one, interpret the command (`accent("'") --> [39].` interpret the `\'{X}` command). The second one, parse its value (``accented_vowel("'", `á`) --> "a".`` parses the `\'{a}` LaTeX code).


## Sorting

Sorting citations by author name or other criteria is not supported&#x2026; Sorry.


# License

![img](https://www.gnu.org/graphics/gplv3-127x51.png)

This software is under the General Public Licence version 3.

See COPYING.txt file or [the GNU GPL Licence page](<https://www.gnu.org/licenses/gpl.html>).

