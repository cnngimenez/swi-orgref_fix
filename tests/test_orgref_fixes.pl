/*   test_orgref_fixes
     Author: Christian Gimenez.

     Copyright (C) 2020 Christian Gimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     11 Jul 2020
*/


:- module(test_orgref_fixes, [	      
	  ]).
/** <module> test_orgref_fixes: Test for orgref_fixes.pl

@license GPLv3
@author Christian Gimenez
*/

:- use_module('../prolog/orgref_fixes').
:- use_module(library(bibtex)).
:- use_module(data).

:- begin_tests(orgref_fixes, []).

test(generate_html) :-
    chang_html(Html_expected),
    bibtex_get_entries('data/biblio.bib',
                       ["chang18:_scaling_knowled_access"], [Entry]),
    orgref_fixes:generate_html(Entry, Html_current),
    Html_expected = Html_current.
                             

test(generate_bibliography) :-
    lst_citations(Citations),
    bibliography_html(Html_expected),
    generate_bibliography('data/biblio.bib', Citations, Html_current),

    %% tell('test_output.html'),
    %% format('~s', [Html_current]),
    %% told,

    Html_current = Html_expected.


test(generate_cites) :-
    lst_citations(Citations),
    cites_expected(Map),
    generate_cites('data/biblio.bib', Citations, Map_current, _),
    %% print(Map_current),
    Map_current = Map.

test(generate_cites_entries) :-
    lst_citations(Citations),
    example_bibtex(Expected_entries),
    generate_cites('data/biblio.bib', Citations, _, Current_entries),
    %% print(Current_entries),
    Current_entries = Expected_entries.
    

test(html_fix_citations) :-
    cites_expected(Citemaps),

    read_file_to_string('data/example.html', Html, []),
    read_file_to_string('data/html_fix_citations-example.html',
                        Expected_html, []),
    
    orgref_fixes:html_fix_citations(Citemaps, Html, Current_html),

    %% tell('test_output.html'),
    %% write(Current_html),
    %% told,
    
    Current_html = Expected_html.

test(insert_bibliography) :-
    example_bibtex(Lst_entries),

    read_file_to_string('data/example.html', Html, []),
    read_file_to_string('data/insert_bibliography-example.html',
                        Expected_html, []),
    
    orgref_fixes:insert_bibliography(Lst_entries, Html, Current_html),
    
    Current_html = Expected_html.

test(fix_citations) :-
    read_file_to_string('data/example.html', Html, []),
    read_file_to_string('data/fix_citations-example.html',
                        Expected_html, []),

    fix_citations('data/example.org', 'data/biblio.bib', Html, Current_html),
    %% tell('try.html'),write(Current_html), told,
    Expected_html = Current_html.

:- end_tests(orgref_fixes).
