/*   pack
     Author: Gimenez, Christian.

     Copyright (C) 2020 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     08 Jul 2020
*/


name(orgref_fix).
title('Fix Org-ref citations.').
version('0.1.2').
author('Christian Gimenez', 'christian.gimenez@fi.uncoma.edu.ar').
home('https://bitbucket.org/cnngimenez/swi-orgref_fix').
download('https://bitbucket.org/cnngimenez/swi-orgref_fix/get/orgref_fix-*.zip').

requires(bibtex).
