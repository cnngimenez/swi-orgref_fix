/*   test
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     09 Jul 2020
*/

:- module(test_orgref_search, []).

/** <module> test_orgref_search: Test for orgref_search module.

@license GPLv3
@author Christian Gimenez
*/

:- license(gplv3).

:- use_module('../prolog/orgref_search').
%% :- use_module(library(bibtex)).
:- use_module(data).

:- begin_tests(orgref_search, []).

test(search_citation) :-
    read_file_to_string('data/example.org', Org, []),

    search_citations(Org, LstCitations),

    lst_citations(LstCitations).

test(search_refs) :-
    read_file_to_string('data/example.org', Org, []),
    search_refs(Org, Lst),
    Lst = [
        "fig:ej_grafo"
    ].

:- end_tests(orgref_search).
